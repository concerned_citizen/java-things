import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

/*=================
	La shitcode
===================*/
class Pair implements Comparable<Pair>{
	public int index;
	public int value;
	public Pair(int index, int value){
		this.index=index;
		this.value=value;
	}
	@Override
	public int compareTo(Pair anotherPair){
		return this.index-anotherPair.index;
	}
	@Override
	public boolean equals(Object other){
		if (other == null) return false;
		if (other == this) return true;
		if (!(other instanceof Pair))return false;
		Pair otherPair = (Pair)other;
		if(this.index==otherPair.index)
			return true;
		else
			return false;
	}
	@Override
	public int hashCode(){
		return 21*this.index;
	}
}
class PairValueComparator implements Comparator<Pair>{ // PairIndexComparator<Pair> implements Comparator<Pair>
	@Override
	public int compare(Pair pair1, Pair pair2){
		return pair1.value-pair2.value;
	}
}

class Lab2FileHandler {
	static ArrayList<String> readCSVFile(String filename) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		try {
			String line = br.readLine();
			ArrayList<String> list = new ArrayList<String>();

			while (line != null) {
				list.add(line);
				line = br.readLine();
			}
			return list;
		} finally {
			br.close();
		}
	}

	static void writeCSVFile(String filename, String content) {
		try {
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			writer.println(content);
			writer.close();
		} catch (Throwable e) {
			System.out.println("Oops");
		}
	}
}


public class Lab2{
	public int[][] Q_matrix;
	public int[][]	R_matrix;
	public int[][]	P_matrix;
	/*
	int[][] Pnew_matrix;
	int[][] C_matrix;*/
    public Lab2(int[][] Q_matrix){
        this.Q_matrix=Q_matrix;
        this.R_matrix=calculateR(this.Q_matrix);
		this.P_matrix=calculateP(new int[5][4]);
    }
//tyt chet ne tak
	int[][] calculateP(int[][] positions){

		int[][] P_matrix=new int[positions.length*positions[0].length][positions.length*positions[0].length];
		for(int i=0;i<positions.length;i++){
			for(int j=0;j<positions[0].length;j++){
				for(int k=0;k<positions.length;k++){
					for(int l=0;l<positions[0].length;l++){
						P_matrix[i*(positions.length-1)+j][k*(positions.length-1)+l]=Math.abs((i-k))+Math.abs((j-l));
								P_matrix[k*(positions.length-1)+l][i*(positions.length-1)+j]=Math.abs((i-k))+Math.abs((j-l));
					}
				}
			}
		}
		this.P_matrix=P_matrix;
		return P_matrix;
	}
	int[][] calculateNewP(){
		int[][] Pnew_matrix;
		int[][] P_matrix=this.P_matrix;
		ArrayList<Pair> Di =Lab2.sortVertexAsc(P_matrix);
		Collections.reverse(Di);
		ArrayList<Pair> Ri=Lab2.sortVertexAsc(this.R_matrix);

		ArrayList<Pair> correspond = new ArrayList<>();
		for(int i=0;i<Ri.size();i++){
			correspond.add(new Pair(Ri.get(i).index,Di.get(i).index)); //in this Pair List index is the E vertex index and value is the corresponding position index.
		}
		Collections.sort(correspond);
		Pnew_matrix=new int[correspond.size()][correspond.size()];
		for(int i=0;i<correspond.size();i++){
			for(int j=i;j<correspond.size();j++){
				Pnew_matrix[i][j]=P_matrix[correspond.get(i).value][correspond.get(j).value];
				Pnew_matrix[j][i]=P_matrix[correspond.get(i).value][correspond.get(j).value];
			}
		}
		return Pnew_matrix;
	}
	int[][] calculateC(int[][] R_m,int [][] P_m){
		int[][] C_m=new int[R_m.length][R_m.length];
		for(int i=0;i<C_m.length;i++){
			for(int j=0;j<C_m.length;j++){
				C_m[i][j]=R_m[i][j]*P_m[i][j];
				C_m[j][i]=C_m[i][j];
			}
		}
		return C_m;
	}
	String[][] shortestDijkstra(int[][] C_matr){
		int[][] shortest_way = new int[C_matr.length][C_matr.length];
		List<List<String>> shortest_way_result = new ArrayList<List<String>>();
		List<Pair> way_coordinates = new ArrayList<Pair>();
		List<Integer> row = new ArrayList<Integer>();

		for(int i = 0;i<C_matr.length;i++){
			if(i==0)
				row.add(0,0);
			else
				row.add(i,-1);
		}
		int const_mark=0;//0+
		way_coordinates.add(new Pair(0,0));
		for(int i=0;i<shortest_way.length;i++){
			int min=-1;
			int j_coord;//there goes PLUS element
			for(int j=0;j<shortest_way[0].length;j++){
				if(C_matr[i][j]!=0) {
					if ((Integer.valueOf(C_matr[i][j] + const_mark)) < row.get(j)) {//Integer.valueOf(C_matr[i][j])<row.get(j)
						row.add(j, C_matr[i][j] + const_mark);
					}
					if(min<0){
						min=C_matr[i][j]+const_mark;
						way_coordinates.add(i,j);
						j_coord=j;
					}
					else if((C_matr[i][j]+const_mark)<min){
						min=C_matr[i][j]+const_mark;
						j_coord=j;
					}

				}//there is something else about eigher -1 or the last value
			}
			List<String> row_string=new ArrayList<>();
			for(int i=0;i<row.size();i++){
				if(i==j_coord)
					row_string.add(Integer.toString(row.get(i))+"+");
				else if(row.get(i)<0){
					row_string.add("-");
				}
				else
					row_string.add(Integer.toString(row.get(i)));
			}
			shortest_way_result.add(row_string);

		}

		return shortest_way_result;
	}

	static ArrayList<Pair> sortVertexAsc(int[][] matrix){
		ArrayList<Pair> seq= new ArrayList<>();
		seq=new ArrayList<Pair>();
		for(int i=0;i<matrix.length;i++){
			int summ=0;
				for(int j=0;j<matrix[0].length;j++){
						summ+=matrix[i][j];
				}
				seq.add(new Pair(i,summ));
		}
		Collections.sort(seq,new PairValueComparator());
		return seq;
	}


	public  static ArrayList<Integer> getSameValueIndex(int index, ArrayList<Pair> pairs){
		ArrayList<Integer> indexes = new ArrayList<>();
		for (Pair pair : pairs){
			if(pair.index==index)
				indexes.add(pair.index);
		}
		return indexes;
	}
	public static Boolean checkIfAdjacent(int index, ArrayList<Pair> pairs, int[][] matrix){
		
		for(Pair pair : pairs){
			if(matrix[pair.index][index]!=0)
				return true;
		}
		return false;
	}
	static int[][] getMatrixFromCsv(ArrayList<String> csvRep){
		int[][] matrix = new int[csvRep.size()][];
		for(int i=0;i<csvRep.size();i++){
			String[] rowString = csvRep.get(i).split(";",-1);
			int[] rowInt = new int[rowString.length];
			for(int j=0;j<rowString.length;j++){
				if(rowString[j].equals(""))
					rowInt[j]=0;
				else
					rowInt[j]=Integer.parseInt(rowString[j]);
			}
			matrix[i]=rowInt;
		}
	
	return matrix;
	}
	static int[][] calculateR(int[][] Q_matrix){
		int e_size = Q_matrix.length;
		int u_size = Q_matrix[0].length;
		int[][] R_matrix=new int[e_size][e_size];
		for(int i=0;i<e_size;i++){
			for(int j=0;j<e_size;j++){
				if(i!=j)
				{
					int summ=0;
					for(int k=0;k<u_size;k++){
						summ+=Q_matrix[i][k]*Q_matrix[j][k];
					}
					R_matrix[i][j]=summ;
				}
				else
				R_matrix[i][j]=0;
				
			}
		}
		return R_matrix;
	}
	public int[][] getR_matrix(){
		return this.R_matrix;
	}

	public ArrayList<Pair> paint(){
		int[][] R_matrix=this.getR_matrix();

		ArrayList<Pair> colored = new ArrayList<>();
        ArrayList<Pair> R_seq;
		int color=1;
		do{
            R_seq=new ArrayList<Pair>();
			for(int i=0;i<R_matrix.length;i++){
				int summ=0;
                if(!(colored.contains(new Pair(i,0)))){
                    for(int j=0;j<R_matrix[0].length;j++){
                        if(!(colored.contains(new Pair(i,0))))
                        summ+=R_matrix[i][j];
                    }
				R_seq.add(new Pair(i,summ));
                }
			}
			Collections.sort(R_seq,new PairValueComparator());
			Collections.reverse(R_seq);

			System.out.print("color"+Integer.toString(color)+": ");
			ArrayList<Pair> currentColor = new ArrayList<>();
			for(Pair pair : R_seq){//TODO:eliminate adjacent vertex. It is easier to make a method for finding non-adjacent vertex. I also probably should make my own List.
				if(!(currentColor.contains(pair) || Lab2.checkIfAdjacent(pair.index,currentColor,R_matrix/*А не Q ли?*/) )){
					currentColor.add(new Pair(pair.index,color));
					//System.out.print("E"+Integer.toString(pair.index)+";");
				}
			}   //TODO: it shall check if it is adjacent with painted of the SAME color. Might be better idea to just create local pair arraylist for the same color and add to the colored afterwards
			colored.addAll(currentColor);
            color++;
		}while(!R_seq.isEmpty());

        System.out.println();
		Collections.sort(colored, new PairValueComparator());
		return colored;
	}

	static void outputFile(int[][] R_matrix) throws FileNotFoundException,UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter("C:\\WorkBench\\output.csv", "UTF-8");
		
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<R_matrix.length;i++){
			for(int j=0;j<R_matrix[i].length;j++){
				sb.append(R_matrix[i][j]);
				sb.append(";");
			}
			sb.append("\n");
		}
		writer.println(sb.toString());
		writer.close();
	}
	public static void main(String[] args){


		try{
			System.out.println("Input R matrix:\n");
			ArrayList<String> result=new ArrayList<String>();
            result = Lab2FileHandler.readCSVFile("C:\\WorkBench\\input.csv");
			int[][] matr = Lab2.getMatrixFromCsv(result);
            Lab2 lab2=new Lab2(matr);

            int[][] R_matr =lab2.getR_matrix();
            for(int[] item : R_matr){
                for(int i = 0;i<item.length;i++){
                    System.out.print(String.valueOf(item[i])+";");
                }
                System.out.print("\n");

            }
            ArrayList<Pair> painted = lab2.paint();
            for(Pair pair : painted) {
                System.out.println("C" + Integer.toString(pair.value) + ": " + Integer.toString(pair.index+1));
            }
			System.out.println("P start matr:");
			int[][] P_matr = lab2.P_matrix;
			for(int[] item : P_matr){
				for(int i = 0;i<item.length;i++){
					System.out.print(String.valueOf(item[i])+";");
				}
				System.out.print("\n");

			}
			System.out.println("P new:");
			int[][] Pn_matr = lab2.calculateNewP();
			for(int[] item : Pn_matr){
				for(int i = 0;i<item.length;i++){
					System.out.print(String.valueOf(item[i])+";");
				}
				System.out.print("\n");

			}
			System.out.println("C matrix:");
			int[][] C_matr = lab2.calculateC(lab2.R_matrix,Pn_matr);
			for(int[] item : C_matr){
				for(int i = 0;i<item.length;i++){
					System.out.print(String.valueOf(item[i])+";");
				}
				System.out.print("\n");

			}


		}catch(Throwable e){

		}
		/*try{
		System.out.println("Input R matrix:\n");
		ArrayList<String> result=new ArrayList<String>();
		result = Lab2FileHandler.readCSVFile("input.csv");
		int[][] matr = Lab2.getMatrixFromCsv(result);
		for(int[] item : matr){
			for(int i = 0;i<item.length;i++){
				System.out.print(String.valueOf(item[i])+";");
			}
			System.out.print("\n");	
			
		}
		System.out.print("Q matrix:\n");	
		int[][] R_matr =Lab2.getAdjMatrixFromR(matr);
		for(int[] item : R_matr){
			for(int i = 0;i<item.length;i++){
				System.out.print(String.valueOf(item[i])+";");
			}
			System.out.print("\n");	
			
		}
		QtoR.outputFile(R_matr);
		}catch(Throwable e){
			System.out.println("Unknown Error occurred. Please contact the developer.\n");
		}*/
	}
}