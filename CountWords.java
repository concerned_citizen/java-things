import java.io.*;
import java.util.HashSet;
import java.lang.String;

class FNFilter implements FilenameFilter{

    String mask;
    FNFilter(String mask){
        this.mask=mask;
    }
    public boolean accept(File dir,String name){
        String f=new File(name).getName();
        return f.indexOf(mask) !=-1;
    }
}
 public class CountWords {
     public static void main(String[] args) {
	 	long all_words=0L;
         HashSet<String> words = new HashSet<>();
         BufferedReader file = null;
		 String path = "D:\\test";
         File[] list = new File(path).listFiles(new FNFilter("txt"));
         try {
             for (int i = 0; i < list.length; i++) {
                 file = new BufferedReader(new FileReader(list[i]));
                 StreamTokenizer word = new StreamTokenizer(file);
			
				/* word.resetSyntax();
				word.wordChars(0x23, 0xFF);
				word.whitespaceChars(0x00, 0x20);
				word.quoteChar('"');*/
                 while ((word.nextToken()) != StreamTokenizer.TT_EOF) {
                    if(word.ttype==StreamTokenizer.TT_WORD){
                         words.add(word.sval);
						 all_words++;
                     }
					 else if(word.ttype==StreamTokenizer.TT_NUMBER){
						words.add(word.toString());
						all_words++;
					 }
                 }
             }
         } catch (IOException e) {
             System.out.println(e);
         }
    
         for (String TEXT : words) {
             System.out.println(TEXT);
         }
		 System.out.println("VOC SIZE:" +words.size());
		  System.out.println("ALL SIZE:" +all_words);
     }
 }