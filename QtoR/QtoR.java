import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public class QtoR{
	static ArrayList<String> csvRead(String filename) throws IOException{
	BufferedReader br = new BufferedReader(new FileReader(filename));
    try {
        String line = br.readLine();
		ArrayList<String> list = new ArrayList<String>();

        while (line != null) {
            list.add(line);
            line = br.readLine();
        }
        return list;
    } finally {
        br.close();
    }
	}
	
	static ArrayList<int[]> getMatrixFromCsv(ArrayList<String> csvRep){
		ArrayList<int[]> matrix=new ArrayList<int[]>();
		for(int i=0;i<csvRep.size();i++){
			String[] rowString = csvRep.get(i).split(";");
			int[] rowInt = new int[rowString.length];
			for(int j=0;j<rowString.length;j++){
				if(rowString[j].equals(""))
					rowInt[j]=0;
				else
					rowInt[j]=Integer.parseInt(rowString[j]);
			}
			matrix.add(i,rowInt);
		}
	return matrix;
	}
	static int[][] getAdjMatrixFromR(ArrayList<int[]> Q_matrix){
		int e_size = Q_matrix.size();
		int u_size = Q_matrix.get(0).length;
		int[][] R_matrix=new int[e_size][e_size];
		for(int i=0;i<e_size;i++){
			for(int j=0;j<e_size;j++){
				if(i!=j)
				{
					int summ=0;
					for(int k=0;k<u_size;k++){
						summ+=Q_matrix.get(i)[k]*Q_matrix.get(j)[k];
					}
					R_matrix[i][j]=summ;
				}
				else
				R_matrix[i][j]=0;
				
			}
		}
		return R_matrix;
	}
	static void outputFile(int[][] R_matrix) throws FileNotFoundException,UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter("output.csv", "UTF-8");
		
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<R_matrix.length;i++){
			for(int j=0;j<R_matrix[i].length;j++){
				sb.append(R_matrix[i][j]);
				sb.append(";");
			}
			sb.append("\n");
		}
		writer.println(sb.toString());
		writer.close();
	}
	public static void main(String[] args){
		try{
		System.out.println("Input R matrix:\n");
		ArrayList<String> result=new ArrayList<String>();
		result = QtoR.csvRead("input.csv");
		ArrayList<int[]> matr = QtoR.getMatrixFromCsv(result);
		for(int[] item : matr){
			for(int i = 0;i<item.length;i++){
				System.out.print(String.valueOf(item[i])+";");
			}
			System.out.print("\n");	
			
		}
		System.out.print("Q matrix:\n");	
		int[][] R_matr =QtoR.getAdjMatrixFromR(matr);
		for(int[] item : R_matr){
			for(int i = 0;i<item.length;i++){
				System.out.print(String.valueOf(item[i])+";");
			}
			System.out.print("\n");	
			
		}
		QtoR.outputFile(R_matr);
		}catch(Throwable e){
			System.out.println("Oops");
		}
	}
}