package wordcounter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class WordCounter {
  private static final String DISPLAY_HEATMAP = "-h";
  private static final String COUNT_NUMBERS_AS_SINGLE_WORD = "-n";
  /**
   * Аккумулирует в себе слова. Класс принимает слова и составляет словарь с картой частоты встреченых слов
   */
  private static final class WordDictionary {
    private int wordsEncountered = 0;
    private final HashMap<String, Integer> words = new HashMap<>();

    /**
     * Добавить слово в словарь. Если слово уже есть, то увеличивает частоту встречи слова
     * @param word слово
     */
    public void accept(String word) {
      if (words.containsKey(word)) {
        words.put(word, words.get(word) + 1);
      } else {
        words.put(word, 1);
      }
      wordsEncountered++;
    }

    /**
     * Размер словаря
     * @return Число уникальных слов переданых словарю
     */
    public int getSize() {
      return words.size();
    }

    /**
     * Число слов переданное в словарь
     * @return число слов
     */
    public int getWordsEncountered() {
      return wordsEncountered;
    }

    /**
     *  Частота встречи слов
     * @return Уникальные слова к числу раз они были встречены
     */
    public Map<String, Integer> getHeatMap() {
      Map<String, Integer> heatmap = new LinkedHashMap<>();
      words.entrySet().stream()
          .sorted(Map.Entry.comparingByValue())
          .forEach(entry -> heatmap.put(entry.getKey(), entry.getValue()));
      return heatmap;
    }
  }

  public static void main(String[] args) {
    // Параметры переданые программе
    Set<String> commandLineOptions = Set.of(args);

    //Ввод пользователя
    System.out.println("Enter directory path:");
    String path = System.console().readLine();
    System.out.println("Enter extension(enter nothing to skip):");
    String extension = System.console().readLine();

    // Фильтруем файлы в директории если нужно
    File[] fileList = new File(path).listFiles(extension.isEmpty()
        ? null
        : (dir, filename) -> filename.endsWith("." + extension));

    boolean treatNumbersAsSingleWord = commandLineOptions.contains(COUNT_NUMBERS_AS_SINGLE_WORD);
    WordDictionary dictionary = new WordDictionary();
    for (File file : fileList) {
      try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
        StreamTokenizer word = new StreamTokenizer(reader);
        while ((word.nextToken()) != StreamTokenizer.TT_EOF) {
          if (word.ttype == StreamTokenizer.TT_WORD) {
            dictionary.accept(word.sval);
          } else if (word.ttype == StreamTokenizer.TT_NUMBER) {
            dictionary.accept(treatNumbersAsSingleWord ? "%number%" : String.valueOf(word.nval));
          }
        }
      } catch (IOException e) {
        System.out.printf("An error happened: %s%n", e.getMessage());
      }
      System.out.println("VOCABULARY SIZE: " + dictionary.getSize());
      System.out.println("TOTAL WORD COUNT: " + dictionary.getWordsEncountered());

      // Отобразить карту частоты слов если нужно
      if (commandLineOptions.contains(DISPLAY_HEATMAP)) {
        dictionary.getHeatMap().forEach((word, frequency) -> System.out.println(word + ":" + frequency));
      }
    }
  }
}
